const url = "https://bakesaleforgood.com"


export default {
    async getInitialiDeals() {
        try {
            const response = await fetch(url + "/api/deals");
            const responseJson = await response.json();
            return responseJson
        } catch (error) {
            console.error(error);
        }
    },
    async getDetailDeal(ID) {
        try {
            const response = await fetch(url + "/api/deals/" + ID);
            const responseJson = await response.json();
            return responseJson
        } catch (error) {
            console.error(error);
        }
    },
    async getSearchDeal(SearchTerm) {
        try {
            const response = await fetch(url + "/api/deals/?searchTerm=" + SearchTerm);
            const responseJson = await response.json();
            return responseJson
        } catch (error) {
            console.error(error);
        }
    }


}
