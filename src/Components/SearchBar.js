import React, { Component } from 'react'
import { TextInput, View, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import debounce from 'lodash.debounce'


export default class SearchBar extends React.Component {
    state = {
        searchTerm: null
    }

    debounceSearchDeal = debounce(this.props.onSearchPress, 3000)
    onChangeText = (text) => {
        this.setState({
            searchTerm: text
        }, () => {
            this.debounceSearchDeal(this.state.searchTerm)
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    placeholder="Seach Here"
                    style={{ height: 40, borderColor: 'gray', borderWidth: 2, flex: 10 }}
                    onChangeText={text => this.onChangeText(text)}
                />
                <View style={{ marginLeft: -30, flex: 1, justifyContent: "center" }}>

                    <Icon name="search" size={20} color='grey' />

                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginRight: 10,
        marginTop: 10, marginLeft: 10
    }
})