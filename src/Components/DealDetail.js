import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, ActivityIndicator, ScrollView, TouchableOpacity } from 'react-native'
import ajax from '../Ajax/ajax'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class DealDetail extends Component {
    state = {
        detail_deal: null
    }
    async componentDidMount() {
        const deal = await ajax.getDetailDeal(this.props.deal.key)
        this.setState({
            detail_deal: deal
        })
    }
    onbackpress = () => {
        this.props.onBackPress()
    }
    render() {

        if (this.state.detail_deal) {

            return (<View >
                <View style={styles.header}>
                    <TouchableOpacity onPress={this.onbackpress}>
                        <Icon name="arrow-left" size={20} color='white' />
                    </TouchableOpacity>

                    <Text style={styles.header_text}>
                        {
                            this.state.detail_deal.title
                        }
                    </Text>

                </View>
                <ScrollView >

                    <View style={styles.card}>
                        <Image source={{ uri: this.state.detail_deal.media[0] }}
                            style={styles.image_Style} />
                        <View style={{ padding: 10, borderTopWidth: 2, borderTopColor: '#bcbcbd' }}>
                            <Text style={[styles.header_text, { fontWeight: "bold" }]}>
                                {
                                    this.state.detail_deal.title
                                }
                            </Text>
                            <View style={{ paddingTop: 5, flexDirection: "row-reverse", justifyContent: "space-between" }}>
                                <Text style={styles.header_text}>
                                    {'$' + this.state.detail_deal.price}
                                </Text>
                                <Text style={styles.header_text}>
                                    {this.state.detail_deal.cause.name}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ margin: 10 }}>
                        <View style={{ flexDirection: "row" }}>
                            <Image style={{ height: 60, width: 60 }} source={{ uri: this.state.detail_deal.user.avatar }} />
                            <Text style={{ marginLeft: 10 }} >
                                {this.state.detail_deal.user.name}
                            </Text>
                        </View>
                        <View style={{ marginTop: 10 }}>
                            <Text>
                                Description
                            </Text>
                            <Text>
                                {this.state.detail_deal.description}
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
            )
        }
        return (
            <View style={styles.ActivityIndicatorStyle}>
                <ActivityIndicator
                    color="#009688"
                    size="large"
                />
                <Text>
                    Loading
                </Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    header: {
        height: 50,
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "#b5651d",
        flexDirection: "row-reverse",
        paddingLeft: 10,
        paddingRight: 10
    },
    header_text: {
        color: "white",
        fontSize: 15,
        paddingLeft: 5
    },
    image_Style: {
        width: "100%",
        height: 200,
    },
    card: {
        borderWidth: 2,
        borderColor: "#bcbcbd",
        backgroundColor: '#b5651d'
    },
    ActivityIndicatorStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})