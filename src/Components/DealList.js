import React, { Component } from 'react'
import { View, Text, StyleSheet, SafeAreaView, FlatList } from 'react-native'
import DealItem from './DealItem'

export default class DealList extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.props.deal}
                    renderItem={({ item }) => (
                        <DealItem deal={item} onPress={this.props.onPress} />
                    )}
                    keyExtractor={item => item.key}
                />
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingTop: 10
    },
    text_tittle: {
        paddingTop: 2
    }
})