import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
export default class DealItem extends Component {

    onItempress = () => {
        this.props.onPress(this.props.deal.key)
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.image_view}
                    onPress={this.onItempress}>
                    <Image source={{ uri: this.props.deal.media[0] }}
                        style={styles.image_Style} />
                    <View style={{ padding: 10, borderTopWidth: 2, borderTopColor: '#bcbcbd' }}>
                        <Text style={{ fontWeight: "bold" }}>
                            {
                                this.props.deal.title
                            }
                        </Text>
                        <View style={{ paddingTop: 5, flexDirection: "row-reverse", justifyContent: "space-between" }}>
                            <Text>
                                {'$' + this.props.deal.price}

                            </Text>
                            <Text>
                                {this.props.deal.cause.name}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginRight: 20,
        marginLeft: 20
    },
    image_Style: {
        width: "100%",
        height: 200,
    },
    image_view: {
        borderWidth: 2,
        borderColor: "#bcbcbd",
        backgroundColor: 'white'
    }
})