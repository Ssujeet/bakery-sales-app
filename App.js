/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

import ajax from './src/Ajax/ajax'
import DealList from './src/Components/DealList'
import DealDetail from './src/Components/DealDetail'
import Searchbar from './src/Components/SearchBar'

class App extends React.Component {
  state = {
    deal: [],
    searchFormDeal: [],
    currentdealID: null,
  }

  async componentDidMount() {
    const deals = await ajax.getInitialiDeals()
    this.setState({
      deal: deals
    })
  }
  searchdeal = async (searchTerm) => {
    let search_deal = []
    if (searchTerm) {
      search_deal = await ajax.getSearchDeal(searchTerm)
    }
    this.setState({
      searchFormDeal: search_deal
    })
  }
  onItempress = (ID) => {
    console.log(ID)
    this.setState({
      currentdealID: ID
    })
  }
  onback_press = () => {
    this.setState({
      currentdealID: null
    })
  }
  currentdeal = () => {
    return this.state.deal.find((deal) => deal.key === this.state.currentdealID);
  }
  render() {
    if (this.state.currentdealID) {
      return <DealDetail deal={this.currentdeal()} onBackPress={this.onback_press} />
    }

    const dealstodisplay = this.state.searchFormDeal.length > 0 ? this.state.searchFormDeal : this.state.deal

    if (dealstodisplay.length > 0) {
      return (
        <View style={styles.container}>
          <Searchbar onSearchPress={this.searchdeal} />
          <DealList deal={dealstodisplay} onPress={this.onItempress} />
        </View>

      )
    }
    return (
      <View style={styles.container}>

        <Text style={styles.bakerytext}>
          Bakery Sale
       </Text>

      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    backgroundColor: '#eeeeee'
  },
  bakerytext: {
    fontSize: 40,
  }
});

export default App;
